open Js

(* Vueオブジェクトのインスタンスパラメーターを定義 *)
let param = 
  let open Unsafe in (* Unsafeをよく使うので開いてしまう *)
  let created () =
(* Javascriptのthisの有効範囲がよくわかってないのでその都度取り込む *)
    let this = Unsafe.variable "this" in
    meth_call this "$watch" [| inject (string "branch");
                               inject (fun () ->
                                        (* このthisの書き方でいいのかな *)
                                        meth_call this "fetchData" [||])
                            |] |> ignore
  in
(* truncateとformatDateは実際にはModelの仕事なのでOCamlっぽく書いてみる *)
  let truncate v =
    try
      let str = to_string v in
      let newLine = String.index str '\n' in
      String.sub str 0 newLine |> string
    with _ -> v
  in
  let formatDate v =
    let str = to_string v in
    let rexp = Regexp.regexp "T|Z" in
    Regexp.global_replace rexp str " " |> string
  in
  let fetchData () =
    let this = Unsafe.variable "this" in
    let self = this in
    let branch_js = self##branch in
    Firebug.console##log(branch_js);
    let branch_ml = to_string (branch_js) in
    let url =
      "https://api.github.com/repos/yyx990803/vue/commits?per_page=3&sha="
      ^ branch_ml
    in
    let (>>=) = Lwt.(>>=) in (* 記号を導入*)
    XmlHttpRequest.get url >>= fun r ->
      let msg = r.XmlHttpRequest.content in
(*     let json = Json.unsafe_input (string msg) in *)
(*     Firebug.console##log(json); *)
(* こう書きたいんだけjs_of_ocamlのJsonオブジェクト内の文字列が
   mlstringになってて期待した通りに動かないのでJavascriptのJSONを輸入する *)
      let json = Unsafe.variable "JSON" in
      self##commits <- Unsafe.meth_call json "parse" [| inject (string msg) |];
      Lwt.return msg
  in
  obj [|
    "el", inject (string "#demo");
    "data", inject (obj [| "branch", inject (string "master") |]);
    "created", inject created;
    "filters", inject (obj [| "truncate", inject truncate;
                              "formatDate", inject formatDate |]);
    "methods", inject (obj [| "fetchData", inject fetchData |]);
  |]

(* JS Objectをコンストラクタの引数にとるVueオブジェクトを定義 *)
let vue : 'a constr = Unsafe.variable "Vue"

(* Vueオブジェクトのインスタンスを定義 *)
let demo = jsnew vue(param)

let () =
  ()
