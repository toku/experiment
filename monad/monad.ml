module type MonadRequirements = sig
  type 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
  val return : 'a -> 'a t
end


module OptionMonad = struct
  type 'a t = 'a option

  let bind opt f =
    match opt with
    | Some(x) -> f x
    | None -> None

  let return x = Some x
end


module ListMonad = struct
  type 'a t = 'a list

  let bind lst f = List.concat (List.map f lst)

  let return x = [ x ]

  let ( >>= ) = bind
  let ( >> ) m f = bind m (fun _ -> f ())

  let guard c =
    if c then return ()
    else []

end



let rec range a b =
  if a = b then []
  else a::range (a+1) b

let make =
  List.fold_left (fun x y -> x*10 + y) 0

let solve () =
  ListMonad.(
    range 1 10 >>= fun a ->
    range 0 10 >>= fun b ->
    range 0 10 >>= fun c ->
    range 0 10 >>= fun d ->
    range 0 10 >>= fun e ->
    let x = make [a;7;b;6;c] in
    let y = make [3;d;2;9;e;6] in
    guard (x * 7 = y) >>= fun _ ->
    return (x, y)
    )

let () =
    let ans = solve () in
    List.iter (fun (a,b) ->
        Printf.printf "%d,%d\n" a b) ans;
