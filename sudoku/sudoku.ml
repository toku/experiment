(* Sudoku solver in OCaml. *)
(* Original source code is written by Haskell. *)
(* http://web.math.unifi.it/~maggesi/haskell_sudoku_solver.html *)

let (@@) f x = f x
let ($) g f = fun x -> g (f x)
let id x = x
let tee f x = ignore @@ f x; x
let (|>) x f = f x
 
(*
import Data.List

type T = (Int,Int) -> [Int]

main = do
  s <- getContents
  putStr $ unlines $ map disp $ solve [input s]

solve :: [T] -> [T]
solve ts = foldr search ts idx where
    search p l = [mark (p,n) s | s <- l, n <- s p]

mark :: ((Int,Int),Int) -> T -> T
mark (p@(i,j),n) s q@(x,y) =
  if p==q then [n] else
  if x==i || y==j || e x i && e y j then delete n $ s q else s q
  where e a b = div (a-1) 3==div (b-1) 3

disp :: T -> String
disp s  = unlines [unwords [show $ head $ s (i,j) | j <- [1..9]] | i <- [1..9]]

input :: String -> T
input s = foldr mark (const [1..9]) $
  [(p,n) | (p,n) <- zip idx $ map read $ lines s >>= words, n>0]

idx :: [(Int,Int)]
idx = [(i,j) | i <- [1..9], j <- [1..9]]
 *)

type t = (int * int) -> int list

(* [1..9] *)
(*
let list19 =
  let aux = ref [] in
  for i=1 to 9 do
    aux := i :: !aux
  done;
  List.rev !aux
 *)
let list19 = [1; 2; 3; 4; 5; 6; 7; 8; 9]

(* const *)
let const a _ = a

(*
mark :: ((Int,Int),Int) -> T -> T
mark (p@(i,j),n) s q@(x,y) =
  if p==q then [n] else
  if x==i || y==j || e x i && e y j then delete n $ s q else s q
  where e a b = div (a-1) 3==div (b-1) 3
 *)
let mark pn s q (*: int list*) =
  let rec delete x = function
    | [] -> []
    | y :: ys ->
        if x = y then ys
        else y :: delete x ys
  in
  let e a b =
    (a-1) / 3 = (b-1) / 3
  in 
  let p, n = pn in
  let i, j = p in
  let x, y = q in
  if p = q then [n] else
  if x = i || y = j || (e x i) && (e y j) then
    delete n @@ s q
  else
    s q

(*
input :: String -> T
input s = foldr mark (const [1..9]) $
  [(p,n) | (p,n) <- zip idx $ map read $ lines s >>= words, n>0]
 *)
let input () =
  let aux = ref [] in
  for i=1 to 9 do
    for j=1 to 9 do
      let n = Scanf.scanf "%d " id in
      if n > 0 then
        aux := ((i,j),n) :: !aux
    done
  done;
  let pns = List.rev !aux in
  let t = List.fold_right mark pns (const list19) in
  t
  
(*
solve :: [T] -> [T]
solve ts = foldr search ts idx where
    search p l = [mark (p,n) s | s <- l, n <- s p]
*)
let solve ts =
  let search p l =
    let f s =
      let ns = s p in
      List.map (fun n ->
                  mark (p, n) s) ns
    in
    List.concat @@ List.map f l
  in
  let aux = ref [] in
  for i=1 to 9 do
    for j=1 to 9 do
      aux := (i,j) :: !aux
    done
  done;
  let idx = List.rev !aux in
  List.fold_right search idx ts 

(*
disp :: T -> String
disp s  = unlines [unwords [show $ head $ s (i,j) | j <- [1..9]] | i <- [1..9]]
 *)
let disp s =
  for i=1 to 9 do
    for j=1 to 9 do
      let p = s (i,j) |> List.hd in
      Printf.printf "%d " p
    done;
    Printf.printf "\n"
  done;
  Printf.printf "\n"

let () =
  let ts = solve [input ()] in
  List.iter disp ts

